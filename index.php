<?php

class RevertService
{
    protected $nodeList;
    protected $nodeListReverted = null;

    public function __construct(Node $list)
    {
        $this->nodeList = $list;
    }

    public function revert(): ?Node
    {
        while ($this->nodeList){
            $node = $this->popNode($this->nodeList);
            if(empty($this->nodeListReverted)){
                $this->nodeListReverted = $node;
            }else{
                $this->addNode($node, $this->nodeListReverted);
            }
        }

        return $this->nodeListReverted;
    }

    protected function popNode(Node $node, ?Node $prevNode = null): Node
    {
        if($node->next()){
            return $this->popNode($node->next(), $node);
        }
        if($prevNode){
            $prevNode->setNextNode(null);
        }else{
            $this->nodeList = null;
        }
        return $node;
    }

    protected function addNode(Node $node, Node $nodeList): void
    {
        if($nodeList->next()){
            $this->addNode($node, $nodeList->next());
            return;
        }

        $nodeList->setNextNode($node);
    }
}

class Node
{
    protected $list = null;
    public $title = null;

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function next(): ?Node
    {
        return $this->list;
    }

    public function setNextNode(?Node $list)
    {
        $this->list = $list;
    }
}
$a = new Node('a');
$b = new Node('b');
$c = new Node('c');
$d = new Node('d');
$e = new Node('e');

$a->setNextNode($b);
$b->setNextNode($c);
$c->setNextNode($d);
$d->setNextNode($e);
var_dump($a);
$revert = new RevertService($a);
$revertNodes = $revert->revert();
var_dump($revertNodes);